<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('admin.login');
})->name('logar');

Route::get('resetSenha', function () {
    return view('admin.reset');
})->name('reset');

Route::get('/dashboard', function () {
    return view('admin.dashboard');
})->name('admin');

Route::get('/fornecedores', function () {
    return view('admin.fornecedor');
})->name('fornecedor');

Route::get('/mantenedores', function () {
    return view('admin.mantenedor');
})->name('mantenedor');

Route::get('/parametros', function () {
    return view('admin.parametros');
})->name('parametros');

Route::get('/prerfil', function () {
    return view('admin.perfil');
})->name('perfil');


//Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

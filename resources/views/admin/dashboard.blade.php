@extends('layout.template')

@section('title', 'Dashboard')


@section('header')
	<!-- C3 charts css -->
	<link href="{{ asset('admin/plugins/c3/c3.min.css') }}" rel="stylesheet" type="text/css"  />
	<style type="text/css" media="screen">
		.preview-iframe{
			border:none;
			width: 100%;
			max-width: 100%;
		}
	</style>
@endsection


@section('content')

<div class="row">
    <div class="col-xs-12 col-lg-12 col-xl-12">
        <div class="card-box">
        <iframe id="snippet-preview" class="preview-iframe" scrolling='no' allowfullscreen src="{{ asset('barra_patrocinio.html') }}"></iframe>
    	</div><!-- end col-->
	</div>
</div>
<!-- end row -->

<div class="row">
    <div class="col-xs-12 col-lg-12 col-xl-12">
        <div class="card-box">
			<div class="row m-t-50">
	            <div class="col-sm-6 col-xs-12 m-t-20">
	                <h4 class="header-title m-t-0" style="text-align: center;">Distribuição dos itens </br>segundo o número de fornecedores.</h4>
	                <div class="p-20">
	                    <div id="RF5-chart"></div>
	                </div>
	            </div>

	            <div class="col-sm-6 col-xs-12 m-t-20">
	                <h4 class="header-title m-t-0" style="text-align: center;">Distribuição dos itens </br>sem fornecedores por fase.</h4>
	                <div class="p-20">
	                    <div id="RF6-chart"></div>
	                </div>
	            </div>
        	</div>

        	<div class="row m-t-50">
	            <div class="col-sm-6 col-xs-12 m-t-20">
	                <h4 class="header-title m-t-0" style="text-align: center;">Evolução do número de fornecedores </br>por item fase de exploração.</h4>
	                <div class="p-20">
	                    <div id="RF7-chart"></div>
	                </div>
	            </div>

	            <div class="col-sm-6 col-xs-12 m-t-20">
	                <h4 class="header-title m-t-0" style="text-align: center;">Evolução do número de fornecedores </br>por item fase de desenvolvimento da produção.</h4>
	                <div class="p-20">
	                    <div id="RF8-chart"></div>
	                </div>
	            </div>
	            <div class="col-sm-6 col-xs-12 m-t-20">
	                <h4 class="header-title m-t-0" style="text-align: center;">Evolução do número de fornecedores </br>por item fase complementar.</h4>
	                <div class="p-20">
	                    <div id="RF9-chart"></div>
	                </div>
	            </div>
        	</div>
    	</div><!-- end col-->
	</div>

</div>
<!-- end row -->                  
                
@endsection


@section('footer')
	<!--C3 Chart-->
	<script type="text/javascript" src="{{ asset('admin/plugins/d3/d3.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('admin/plugins/c3/c3.min.js') }}"></script>
	<script src="{{ asset('admin/pages/jquery.c3-chart.init.js') }}"></script>
@endsection

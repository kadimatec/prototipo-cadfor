@extends('layout.template')

@section('title', 'Mantenedor')


@section('header')
	<style type="text/css" media="screen">
		.preview-iframe{
			border:none;
			width: 100%;
			max-width: 100%;
		}
	</style>
@endsection


@section('content')

<div class="row">
    <div class="col-xs-12 col-lg-12 col-xl-12">
        <div class="card-box">
        <iframe id="snippet-preview" class="preview-iframe" scrolling='no' allowfullscreen src="{{ asset('barra_patrocinio.html') }}"></iframe>
    	</div><!-- end col-->
	</div>
</div>
<!-- end row --> 

<div class="row">
    <div class="col-xs-12 col-lg-12 col-xl-12">
        <div class="card-box">
			
    	</div><!-- end col-->
	</div>
</div>
<!-- end row -->                  
                
@endsection


@section('footer')

@endsection

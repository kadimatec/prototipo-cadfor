<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Sistema CADFOR">
        <meta name="author" content="Localsoft">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- App Favicon -->
        <link rel="shortcut icon" href="{{ asset('admin/images/favicon.ico') }}">

        <!-- App title -->
        <title>{{ config('app.name', 'CADFOR') }} - Recuperar Senha</title>

        <!-- Switchery css -->
        <link href="{{ asset('admin/plugins/switchery/switchery.min.css') }}" rel="stylesheet" />

        <!-- Bootstrap CSS -->
        <link href="{{ asset('admin/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />

        <!-- App CSS -->
        <link href="{{ asset('admin/css/style.css') }}" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js') }}"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js') }}"></script>
        <![endif]-->
        <!-- Modernizr js -->
        <script src="{{ asset('admin/js/modernizr.min.js') }}"></script>
    </head>
    <body>

        <div class="account-pages"></div>
        <div class="wrapper-page">

        	<div class="account-bg">
                <div class="card-box mb-0">
                    <div class="text-center m-t-20">
                        <img src="{{ asset('admin/images/LogoONIPm.png') }}" width="70px">
                    </div>
                    <div class="m-t-10 p-20">

                        <div class="row">
                            <div class="col-12 text-center">
                                <h6 class="text-muted text-uppercase mb-0 m-t-0">Reset Password</h6>
                                <p class="text-muted m-b-0 font-13 m-t-20">Digite seu endereço de e-mail e nós lhe enviaremos um e-mail com instruções para redefinir sua senha.</p>
                            </div>
                        </div>

                        <form class="m-t-20" action="{{ route('logar') }}" method="get">
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                            <div class="form-group row">
                                <div class="col-12">
                                    <input class="form-control" type="email" required="" placeholder="Digite seu e-mail">
                                </div>
                            </div>

                            <div class="form-group row text-center m-t-20 mb-0">
                                <div class="col-12">
                                    <button class="btn btn-success btn-block waves-effect waves-light" type="submit">Enviar Email</button>
                                </div>
                            </div>

                        </form>

                    </div>
                </div>
            </div>
            <!-- end card-box-->

        </div>
        <!-- end wrapper page -->


        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="{{ asset('admin/js/jquery.min.js') }}"></script>
        <script src="{{ asset('admin/js/tether.min.js') }}"></script><!-- Tether for Bootstrap -->
        <script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('admin/js/detect.js') }}"></script>
        <script src="{{ asset('admin/js/fastclick.js') }}"></script>
        <script src="{{ asset('admin/js/jquery.blockUI.js') }}"></script>
        <script src="{{ asset('admin/js/waves.js') }}"></script>
        <script src="{{ asset('admin/js/jquery.nicescroll.js') }}"></script>
        <script src="{{ asset('admin/js/jquery.scrollTo.min.js') }}"></script>
        <script src="{{ asset('admin/js/jquery.slimscroll.js') }}"></script>
        <script src="{{ asset('admin/plugins/switchery/switchery.min.js') }}"></script>

        <!-- App js -->
        <script src="{{ asset('admin/js/jquery.core.js') }}"></script>
        <script src="{{ asset('admin/js/jquery.app.js') }}"></script>

    </body>
</html>
/**
Template Name: Uplon Dashboard
Author: CoderThemes
Email: coderthemes@gmail.com
File: C3 Charts
*/

!function($) {
    "use strict";

    var ChartC3 = function() {};

    ChartC3.prototype.init = function () {
        //generating chart 
     var chart = c3.generate({
            bindto: '#RF7-chart',
            data: {
                x: 'x',
                //xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
                columns: [
                    //['x', '01-08-2015', '01-09-2015', '01-10-2015', '01-11-2015', '01-12-2015', '01-01-2016'],
                    ['x', '2015-08-01', '2015-09-01', '2015-10-01', '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01', '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01', '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01', '2016-12-01'],
                    //['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
                    ['1ª Etapa - I - Geologia & Geofísica', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 100, 100],
                    ['1ª Etapa - II - Completação', 600, 750, 1000, 1400, 1300, 1200, 1300, 1400, 1500, 1400, 1400, 1600, 1650, 1700, 1800, 1850, 1850],
                    ['1ª Etapa - III - Apoio Logístico', 700, 850, 1100, 1500, 1400, 1300, 1400, 1500, 1600, 1500, 1500, 1700, 1750, 1800, 1900, 1950, 1950],
                ]
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%b-%y'
                    }
                }
            },
            color: {
                pattern: ["#1E90FF", "#CD2626", "#32CD32"]
            }
        });

        var chart = c3.generate({
            bindto: '#RF8-chart',
            data: {
                x: 'x',
                //xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
                columns: [
                    //['x', '01-08-2015', '01-09-2015', '01-10-2015', '01-11-2015', '01-12-2015', '01-01-2016'],
                    ['x', '2015-08-01', '2015-09-01', '2015-10-01', '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01', '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01', '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01', '2016-12-01'],
                    //['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
                    //['1ª Etapa - I - Geologia & Geofísica', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 100, 100],
                    ['2ª Etapa - II - Sistema de Coleta da Produção', 3500, 4100, 4200, 5000, 5100, 5200, 5500, 5600, 5700, 5500, 5700, 5800, 6000, 7000, 8000, 8000, 8000],
                    ['2ª Etapa - III - UEP', 400, 600, 700, 900, 900, 900, 1000, 1100, 1000, 1100, 1200, 1300, 1300, 1500, 1600, 1600, 1600],
                ]
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%b-%y'
                    }
                }
            },
            color: {
                pattern: ["#CD2626", "#1E90FF"]
            }
        });

        var chart = c3.generate({
            bindto: '#RF9-chart',
            data: {
                x: 'x',
                //xFormat: '%Y%m%d', // 'xFormat' can be used as custom format of 'x'
                columns: [
                    //['x', '01-08-2015', '01-09-2015', '01-10-2015', '01-11-2015', '01-12-2015', '01-01-2016'],
                    ['x', '2015-08-01', '2015-09-01', '2015-10-01', '2015-11-01', '2015-12-01', '2016-01-01', '2016-02-01', '2016-03-01', '2016-04-01', '2016-05-01', '2016-06-01', '2016-07-01', '2016-08-01', '2016-09-01', '2016-10-01', '2016-11-01', '2016-12-01'],
                    //['x', '20130101', '20130102', '20130103', '20130104', '20130105', '20130106'],
                    //['1ª Etapa - I - Geologia & Geofísica', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 100, 100, 100],
                    ['3ª Etapa - IV - Outros Operacionais'  , 4000, 4400, 4700, 5100, 5600, 5600, 5900, 5900, 6200, 6600, 6900, 7100, 7600, 8000, 8300, 8600, 8900],
                    ['3ª Etapa - V - Outros Administrativos', 3900, 4300, 4600, 5000, 5500, 5500, 5800, 5800, 6100, 6500, 6800, 7000, 7500, 7900, 8200, 8500, 8800],
                ]
            },
            axis: {
                x: {
                    type: 'timeseries',
                    tick: {
                        format: '%b-%y'
                    }
                }
            },
            color: {
                pattern: ["#CD2626", "#1E90FF"]
            }
        });

        //combined chart
        c3.generate({
            bindto: '#combine-chart',
            data: {
                columns: [
                    ['data1', 30, 20, 50, 40, 60, 50],
                    ['data2', 200, 130, 90, 240, 130, 220],
                    ['data3', 300, 200, 160, 400, 250, 250],
                    ['data4', 200, 130, 90, 240, 130, 220],
                    ['data5', 130, 120, 150, 140, 160, 150]
                ],
                types: {
                    data1: 'bar',
                    data2: 'bar',
                    data3: 'spline',
                    data4: 'line',
                    data5: 'bar'
                },
                colors: {
                    data1: '#ebeff2',
                    data2: '#1bb99a',
                    data3: '#ff5d48',
                    data4: '#64b0f2',
                    data5: '#ff7aa3'
                },
                groups: [
                    ['data1','data2']
                ]
            },
            axis: {
                x: {
                    type: 'categorized'
                }
            }
        });
        
        //roated chart
        c3.generate({
            bindto: '#roated-chart',
            data: {
                columns: [
                ['data1', 30, 200, 100, 400, 150, 250],
                ['data2', 50, 20, 10, 40, 15, 25]
                ],
                types: {
                data1: 'bar'
                },
                colors: {
	                data1: '#3db9dc',
	                data2: '#ff5d48'
	            },
            },
            axis: {
                rotated: true,
                x: {
                type: 'categorized'
                }
            }
        });

        //stacked chart
        c3.generate({
            bindto: '#chart-stacked',
            data: {
                columns: [
                    ['data1', 30, 20, 50, 40, 60, 50],
                    ['data2', 200, 130, 90, 240, 130, 220]
                ],
                types: {
                    data1: 'area-spline',
                    data2: 'area-spline'
                    // 'line', 'spline', 'step', 'area', 'area-step' are also available to stack
                },
                colors: {
                    data1: '#ff7aa3',
                    data2: '#9261c6',
                }
            }
        });
        
        //RF5 Chart
        c3.generate({
             bindto: '#RF5-chart',
            data: {
                columns: [
                    ['Zero', '9.9'],
                    ['1 Fornecedor', '12.5'],
                    ['2 Fornecedores', '12.5'],
                    ['3 Fornecedores', '10.6'],
                    ['Mais de 3', '54.5'],
                ],
                type : 'donut'
            },
            donut: {
                title: "",
                width: 70,
				label: { 
					show:true
				}
            },
            color: {
            	pattern: ["#CD2626", "#FFD700", "#32CD32", "#FF4500", "#00BFFF"]
            }
        });

        //RF6 Chart
        c3.generate({
             bindto: '#RF6-chart',
            data: {
                columns: [
                    ['Exploração', '46.5'],                    
                    ['Outros Operacionais', '34.3'],
                    ['Desenvolvimento', '17.6'],
                    ['Outros Administrativos', '1.5'],
                ],
                type : 'donut'
            },
            donut: {
                title: "",
                width: 70,
                label: { 
                    show:true
                }
            },
            color: {
                pattern: ["#00BFFF", "#32CD32", "#FFD700", "#CD2626"]
            }
        });
        
        //Pie Chart
        c3.generate({
             bindto: '#pie-chart',
            data: {
                columns: [
                    ['Lulu', 46],
                    ['Olaf', 24],
                    ['Item 3', 30]
                ],
                type : 'pie'
            },
            color: {
            	pattern: ["#ebeff2", "#ff5d48", "#3db9dc"]
            },
            pie: {
		        label: {
		          show: false
		        }
		    }
        });
        
        //Line regions
        c3.generate({
             bindto: '#line-regions',
            data: {
                columns: [
		            ['data1', 30, 200, 100, 400, 150, 250],
		            ['data2', 50, 20, 10, 40, 15, 25]
		        ],
		        regions: {
		            'data1': [{'start':1, 'end':2, 'style':'dashed'},{'start':3}], // currently 'dashed' style only
		            'data2': [{'end':3}]
		        },
		        colors: {
	                data1: '#039cfd',
	                data2: '#1bb99a'
	            },
            },
            
        });
        
        
        //Scatter Plot
        c3.generate({
             bindto: '#scatter-plot',
             data: {
		        xs: {
		            setosa: 'setosa_x',
		            versicolor: 'versicolor_x',
		        },
		        // iris data from R
		        columns: [
		            ["setosa_x", 3.5, 3.0, 3.2, 3.1, 3.6, 3.9, 3.4, 3.4, 2.9, 3.1, 3.7, 3.4, 3.0, 3.0, 4.0, 4.4, 3.9, 3.5, 3.8, 3.8, 3.4, 3.7, 3.6, 3.3, 3.4, 3.0, 3.4, 3.5, 3.4, 3.2, 3.1, 3.4, 4.1, 4.2, 3.1, 3.2, 3.5, 3.6, 3.0, 3.4, 3.5, 2.3, 3.2, 3.5, 3.8, 3.0, 3.8, 3.2, 3.7, 3.3],
		            ["versicolor_x", 3.2, 3.2, 3.1, 2.3, 2.8, 2.8, 3.3, 2.4, 2.9, 2.7, 2.0, 3.0, 2.2, 2.9, 2.9, 3.1, 3.0, 2.7, 2.2, 2.5, 3.2, 2.8, 2.5, 2.8, 2.9, 3.0, 2.8, 3.0, 2.9, 2.6, 2.4, 2.4, 2.7, 2.7, 3.0, 3.4, 3.1, 2.3, 3.0, 2.5, 2.6, 3.0, 2.6, 2.3, 2.7, 3.0, 2.9, 2.9, 2.5, 2.8],
		            ["setosa", 0.2, 0.2, 0.2, 0.2, 0.2, 0.4, 0.3, 0.2, 0.2, 0.1, 0.2, 0.2, 0.1, 0.1, 0.2, 0.4, 0.4, 0.3, 0.3, 0.3, 0.2, 0.4, 0.2, 0.5, 0.2, 0.2, 0.4, 0.2, 0.2, 0.2, 0.2, 0.4, 0.1, 0.2, 0.2, 0.2, 0.2, 0.1, 0.2, 0.2, 0.3, 0.3, 0.2, 0.6, 0.4, 0.3, 0.2, 0.2, 0.2, 0.2],
		            ["versicolor", 1.4, 1.5, 1.5, 1.3, 1.5, 1.3, 1.6, 1.0, 1.3, 1.4, 1.0, 1.5, 1.0, 1.4, 1.3, 1.4, 1.5, 1.0, 1.5, 1.1, 1.8, 1.3, 1.5, 1.2, 1.3, 1.4, 1.4, 1.7, 1.5, 1.0, 1.1, 1.0, 1.2, 1.6, 1.5, 1.6, 1.5, 1.3, 1.3, 1.3, 1.2, 1.4, 1.2, 1.0, 1.3, 1.2, 1.3, 1.3, 1.1, 1.3],
		        ],
		        
		        type: 'scatter'
		    },
		    color: {
            	pattern: ["#1bb99a", "#f1b53d", "#1bb99a", "#f1b53d"]
            },
		    axis: {
		        x: {
		            label: 'Sepal.Width',
		            tick: {
		                fit: false
		            }
		            
		        },
		        y: {
		            label: 'Petal.Width'
		        }
		    }
            
        });

    },
    $.ChartC3 = new ChartC3, $.ChartC3.Constructor = ChartC3

}(window.jQuery),

//initializing 
function($) {
    "use strict";
    $.ChartC3.init()
}(window.jQuery);



